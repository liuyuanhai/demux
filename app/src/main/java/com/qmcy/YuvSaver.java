package com.qmcy;

import android.os.SystemClock;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class YuvSaver {

    private static final int LOCK_WAIT_FOR_WRITE = 0;
    private static final int LOCK_WRITE_END = 1;
    private static final int LOCK_READ_START = 2;
    private static final int LOCK_READ_END = 3;
    private static final int LOCK_STOP_PLAYING = 4;
    private long mLastUpdateTime;
    private static final int ONE_FRAME_TIME = 30;
    byte[] LOCK = new byte[1];
    private static final String LOCK_FILE = "/data/vendor/.water/lock";
    private static final String NV21_FILE = "/data/vendor/.water/water";
    private boolean mIsRunning;

    public void setRunning(boolean isRunning){
        mIsRunning = isRunning;
    }

    public void onVideoFrame(byte[] nv21){
        long time = SystemClock.elapsedRealtime();
        if(time - mLastUpdateTime < ONE_FRAME_TIME){

            try {
                Thread.sleep(ONE_FRAME_TIME - (time - mLastUpdateTime));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //return;
        }

        FileInputStream mLockInputStream = null;
        FileOutputStream mLockOutputStream = null;
        FileOutputStream mVideoOutputStream = null;
        try {
            mLockInputStream = new FileInputStream(LOCK_FILE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(null != mLockInputStream){
            try {
                do {
                    mLockInputStream.read(LOCK);
                    mLockInputStream.close();
                    if(LOCK[0] != LOCK_READ_START){
                        mLockInputStream = null;
                        break;
                    }
                    try{
                        Thread.sleep(2);
                    }catch (Exception e){

                    }

                }while (mIsRunning);

                mLockOutputStream = new FileOutputStream(LOCK_FILE);
                mLockOutputStream.write(LOCK_WAIT_FOR_WRITE);
                mLockOutputStream.flush();
                mLockOutputStream.close();
                mLockOutputStream = null;

                mVideoOutputStream = new FileOutputStream(NV21_FILE);
                mVideoOutputStream.write(nv21);

                mVideoOutputStream.flush();
                mVideoOutputStream.close();
                mVideoOutputStream = null;

                mLockOutputStream = new FileOutputStream(LOCK_FILE);
                mLockOutputStream.write(LOCK_WRITE_END);
                mLockOutputStream.flush();
                mLockOutputStream.close();
                mLockOutputStream = null;

                mLastUpdateTime = time;

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(null != mLockInputStream){
            try {
                mLockInputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        if(null != mLockOutputStream){
            try {
                mLockOutputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(null != mVideoOutputStream){
            try {
                mVideoOutputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}

package com.qmcy.demux;

import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceHolder;

import com.qmcy.YuvSaver;

import java.io.File;
import java.io.FileOutputStream;

import androidx.appcompat.app.AppCompatActivity;

//这是一个简单的演示Demo
//使用前请授予完整的存储卡访问权限
@SuppressWarnings("all")
public class PlayActivity extends AppCompatActivity implements FFDemuxJava.EventCallback{
    H264PlayView h264PlayView;
    private String mVideoPath = "rtsp://192.168.1.2:8554/main";
//    private String mVideoPath = "rtsp://192.168.32.79:8554/main";
    private FFDemuxJava m_demuxer = null;
    private boolean isGetAFrame;//是否已得到一帧
    YuvSaver yuvSaver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        h264PlayView = findViewById(R.id.h264PlayView);
        mVideoPath = getIntent().getStringExtra("url");
        yuvSaver = new YuvSaver();

        h264PlayView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                m_demuxer = new FFDemuxJava();
                m_demuxer.addEventCallback(PlayActivity.this);
                m_demuxer.init(mVideoPath);
                m_demuxer.Start();
                yuvSaver.setRunning(true);
                h264PlayView.setVideoInfo(500, 500, 30);
                h264PlayView.start();
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
            }
        });

    }
    public static void saveToSDCard(String filePath, String filecontent) throws Exception {
        File file = new File(filePath);
        FileOutputStream outStream = new FileOutputStream(file);
        outStream.write(filecontent.getBytes());
        outStream.close();
    }
    public void byte2image(byte[] data,String path){
        if(data.length<3||path.equals("")) return;
        try{
            FileOutputStream imageOutput = new FileOutputStream(new File(path));
            imageOutput.write(data, 0, data.length);
            imageOutput.close();
            System.out.println("Make Picture success,Please find image in " + path);
        } catch(Exception ex) {
            System.out.println("Exception: " + ex);
            ex.printStackTrace();
        }
    }
    @Override
    protected void onDestroy() {
        if(m_demuxer!=null){
            m_demuxer.stop();
        }
        if(yuvSaver!=null){
            yuvSaver.setRunning(false);
        }
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onMessageEvent(int msgType, float msgValue) {

    }

    @Override
    public void onPacketEvent(byte[] data) {
        h264PlayView.decodeFrame(data,0,data.length);
    }

    @Override
    public void onFrameEvent(byte[] data) {
        Log.e("---------------->","data.size = "+data.length);
        yuvSaver.onVideoFrame(data);
        //得到一帧的数据
        if(!isGetAFrame){
            isGetAFrame = true;
            byte2image(data,"sdcard/test.png");
        }
    }
}


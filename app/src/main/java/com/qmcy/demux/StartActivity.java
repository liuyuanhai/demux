package com.qmcy.demux;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

//按返回键才会启动播放界面
//这样做是为了留一点时间，让Debugger先启动
//Native代码调试，有时代码会比Debugger先执行
public class StartActivity extends AppCompatActivity {

    private EditText et;
    private Button bt_permission,bt_pull;
    @SuppressLint("MissingInflatedId")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
       et = findViewById(R.id.et);
       bt_permission = findViewById(R.id.bt_permission);
       bt_pull = findViewById(R.id.bt_pull);
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.bt_permission:
                        requestPermissions(new String[]{
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.RECORD_AUDIO,
                                Manifest.permission.CAMERA
                        }, 0);
                        break;
                    case R.id.bt_pull:
                        String pupllAddress = et.getText().toString().trim();
                        if(pupllAddress.equals("")){
                            Toast.makeText(StartActivity.this,"请输入拉流地址",Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Intent intent = new Intent(StartActivity.this, PlayActivity.class);
                        intent.putExtra("url",pupllAddress);
                        startActivity(intent);
                        break;
                }
            }
        };
        bt_pull.setOnClickListener(listener);
        bt_permission.setOnClickListener(listener);
    }
}

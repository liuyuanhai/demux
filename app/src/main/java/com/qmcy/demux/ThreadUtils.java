package com.qmcy.demux;

public class ThreadUtils {

    public static void post(Action action) {
        new Thread(() -> {
            action.runAndPostException();
        }).start();
    }

    public interface Action {

        void run() throws Throwable;

        default void runAndPostException() {
            try {
                run();
            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
    }
}
